from datetime import datetime, timedelta
from enum import Enum

DateTimeKind = Enum('DateTimeKind', 'Unspecified Utc Local', start=0)


class DateTime:
    def __init__(self, ticks, kind):
        self.ticks = ticks
        self.CheckDateTimeKind(kind)

    @staticmethod
    def CheckDateTimeKind(kind):
        if kind not in DateTimeKind:
            raise ValueError("Invalid DateTimeKind value.", "kind")

    @classmethod
    def FromBinary(cls, long):
        num = long >> 62
        if num == 0:
            return cls(long, DateTimeKind.Unspecified)
        if not num == 1:
            return cls(long & 4611686018427387903, DateTimeKind.Utc)
        return cls(long ^ 4611686018427387904, DateTimeKind.Utc)

    def to_datetime(self):
        return datetime(1, 1, 1) + timedelta(seconds=self.ticks / 10000000)

