import io
import datetime
import struct
import zipfile

from PIL import Image

import csharputils


def fix_image(image):
    r, g, b, a = image.split()
    return Image.merge('RGBA', (g, b, a, r))


class DemoDrone:
    def __init__(self, *args, **kwargs):
        pass

    @staticmethod
    def read_string(file):
        n = ord(file.read(1))
        return struct.unpack('{}s'.format(n), file.read(n))[0]

    @staticmethod
    def read_float(file):
        return struct.unpack('f', file.read(4))[0]

    @staticmethod
    def read_int(file):
        return struct.unpack('i', file.read(4))[0]

    @staticmethod
    def read_long(file):
        return struct.unpack('l', file.read(4))[0]

    @staticmethod
    def read_long_long(file):
        return struct.unpack('q', file.read(8))[0]

    @staticmethod
    def read_bool(file):
        return struct.unpack('?', file.read(1))[0]

    @staticmethod
    def read_bytes(file, num):
        return file.read(num)

    @classmethod
    def from_file(cls, file):
        with zipfile.ZipFile(file) as zip:
            drone_info = io.BytesIO(zip.read('DroneInfo'))
            _cls = cls()
            _cls.name = DemoDrone.read_string(drone_info).decode()
            _cls.diameter = DemoDrone.read_float(drone_info)
            _cls.numberofparts = DemoDrone.read_int(drone_info)
            _cls.numberofweapons = DemoDrone.read_int(drone_info)
            _cls.hasbeennamed = DemoDrone.read_bool(drone_info)
            DemoDrone.read_bool(drone_info)
            num = DemoDrone.read_int(drone_info)
            if num:
                size = (DemoDrone.read_int(drone_info), DemoDrone.read_int(drone_info))
                try:
                    _cls.image = fix_image(Image.frombytes('RGBA', size, DemoDrone.read_bytes(drone_info, num)))
                except MemoryError:
                    pass
            num = DemoDrone.read_int(drone_info)
            if num:
                size = (DemoDrone.read_int(drone_info), DemoDrone.read_int(drone_info))
                try:
                    _cls.smallimage = fix_image(Image.frombytes('RGBA', size, DemoDrone.read_bytes(drone_info, num)))
                except MemoryError:
                    pass
            _cls.savedate = csharputils.DateTime.FromBinary(DemoDrone.read_long_long(drone_info)).to_datetime()
            count = DemoDrone.read_int(drone_info)
            _cls.parts = []
            for _ in range(count):
                _cls.parts.append(DemoDrone.read_string(drone_info))
            return _cls
